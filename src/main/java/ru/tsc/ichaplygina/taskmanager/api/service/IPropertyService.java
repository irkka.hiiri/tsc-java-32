package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.api.property.IApplicationProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IAutosaveProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IFileScannerProperty;
import ru.tsc.ichaplygina.taskmanager.api.property.IPasswordProperty;

public interface IPropertyService extends IPasswordProperty, IApplicationProperty, IAutosaveProperty, IFileScannerProperty {

}
