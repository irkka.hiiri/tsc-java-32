package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.IAuthRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IAuthService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotLoggedInException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedException;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedNotAuthorizedException;
import ru.tsc.ichaplygina.taskmanager.model.User;

import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class AuthService implements IAuthService {

    @NotNull
    private final IAuthRepository authRepository;

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    public AuthService(
            @NotNull final IAuthRepository authRepository,
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService) {
        this.authRepository = authRepository;
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public final String getCurrentUserId() {
        return authRepository.getCurrentUserId();
    }

    @Override
    public final void setCurrentUserId(@Nullable final String currentUserId) {
        authRepository.setCurrentUserId(currentUserId);
    }

    @NotNull
    @Override
    public final String getCurrentUserLogin() {
        if (isEmptyString(getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        @NotNull final User user = Optional.ofNullable(userService.findById(getCurrentUserId())).orElseThrow(UserNotFoundException::new);
        return user.getLogin();
    }

    @Override
    public final void checkRoles(@NotNull final Role[] roles) {
        if (roles == null) return;
        if (isEmptyString(getCurrentUserId())) return;
        @NotNull final User user = Optional.ofNullable(userService.findById(getCurrentUserId())).orElseThrow(UserNotFoundException::new);
        for (@NotNull final Role role : roles) {
            if (user.getRole().equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Override
    public final boolean isNoUserLoggedIn() {
        return isEmptyString(authRepository.getCurrentUserId());
    }

    @Override
    public final boolean isPrivilegedUser() {
        if (isEmptyString(getCurrentUserId())) return false;
        @NotNull final User user = Optional.ofNullable(userService.findById(getCurrentUserId())).orElseThrow(UserNotFoundException::new);
        return user.getRole().equals(Role.ADMIN);
    }

    @Override
    public final void login(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final User user = Optional.ofNullable(userService.findByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
        if (!user.getPasswordHash().equals(salt(password, propertyService))) throw new IncorrectCredentialsException();
        if (user.isLocked()) throw new AccessDeniedException();
        setCurrentUserId(user.getId());
    }

    @Override
    public final void logout() {
        if (isNoUserLoggedIn()) throw new UserNotLoggedInException();
        setCurrentUserId(null);
    }

    @Override
    public final void throwExceptionIfNotAuthorized() {
        if (isNoUserLoggedIn()) throw new AccessDeniedNotAuthorizedException();
    }

}
