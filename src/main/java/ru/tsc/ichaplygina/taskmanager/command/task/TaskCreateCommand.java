package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class TaskCreateCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "create task";

    @NotNull
    public final static String DESCRIPTION = "create a new task";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @NotNull final String description = readLine(DESCRIPTION_INPUT);
        getTaskService().add(name, description);
        printLinesWithEmptyLine("Done. Type <list tasks> to view all tasks.");
    }

}
