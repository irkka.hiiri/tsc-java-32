package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "show project by id";

    @NotNull
    public final static String DESCRIPTION = "show project by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @Nullable final Project project = getProjectService().findById(id);
        showProject(project);
    }

}
