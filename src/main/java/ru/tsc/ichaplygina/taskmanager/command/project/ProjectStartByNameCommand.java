package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class ProjectStartByNameCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "start project by name";

    @NotNull
    public final static String DESCRIPTION = "start project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        throwExceptionIfNull(getProjectService().startByName(name));
    }

}
