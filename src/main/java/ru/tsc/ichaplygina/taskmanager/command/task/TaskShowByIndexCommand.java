package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "show task by index";

    @NotNull
    public final static String DESCRIPTION = "show task by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        @Nullable final Task task = getTaskService().findByIndex(index - 1);
        showTask(task);
    }

}
