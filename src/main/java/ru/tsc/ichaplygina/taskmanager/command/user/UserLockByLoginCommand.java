package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.security.UserSelfLockNotAllowedException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class UserLockByLoginCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "lock user by login";

    @NotNull
    public static final String DESCRIPTION = "lock user by login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        if (login.equals(getAuthService().getCurrentUserLogin())) throw new UserSelfLockNotAllowedException();
        if (!getUserService().lockByLogin(login)) printLinesWithEmptyLine(USER_ALREADY_LOCKED);
    }

}
