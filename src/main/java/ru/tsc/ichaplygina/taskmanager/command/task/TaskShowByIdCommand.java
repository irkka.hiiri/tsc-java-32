package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Task;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "show task by id";

    @NotNull
    public final static String DESCRIPTION = "show task by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        @Nullable final Task task = getTaskService().findById(id);
        showTask(task);
    }

}
