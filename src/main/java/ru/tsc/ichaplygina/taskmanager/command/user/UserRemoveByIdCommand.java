package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.security.UserSelfDeleteNotAllowedException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class UserRemoveByIdCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "remove user by id";

    @NotNull
    public static final String DESCRIPTION = "remove user by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        if (id.equals(getAuthService().getCurrentUserId())) throw new UserSelfDeleteNotAllowedException();
        throwExceptionIfNull(getUserService().removeById(id));
    }

}
