package ru.tsc.ichaplygina.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;
import ru.tsc.ichaplygina.taskmanager.command.domain.DomainSaveBackupCommand;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    public final static String CMD_NAME = "exit";

    @NotNull
    public final static String DESCRIPTION = "quit";

    @NotNull
    private static final String SAVE_BACKUP_COMMAND = DomainSaveBackupCommand.NAME;

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @Nullable
    @Override
    public final String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        if (getPropertyService().isAutosaveOnExit())
            getCommandService().getCommands().get(SAVE_BACKUP_COMMAND).execute();
        System.exit(0);
    }

}
