package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readRole;

public final class ChangeRoleCommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "change role";

    @NotNull
    public static final String DESCRIPTION = "change user's role";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    public final void execute() {
        @NotNull final String login = readLine(ENTER_LOGIN);
        @NotNull final Role role = readRole(ENTER_ROLE);
        getUserService().setRole(login, role);
    }

}
