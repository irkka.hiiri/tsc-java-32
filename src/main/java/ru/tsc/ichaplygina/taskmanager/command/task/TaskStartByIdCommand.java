package ru.tsc.ichaplygina.taskmanager.command.task;

import org.jetbrains.annotations.NotNull;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "start task by id";

    @NotNull
    public final static String DESCRIPTION = "start task by id";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String id = readLine(ID_INPUT);
        throwExceptionIfNull(getTaskService().startById(id));
    }

}
