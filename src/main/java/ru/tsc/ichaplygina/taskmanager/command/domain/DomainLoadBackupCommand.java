package ru.tsc.ichaplygina.taskmanager.command.domain;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.dto.Domain;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DomainLoadBackupCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "load backup";

    @NotNull
    public final static String DESCRIPTION = "load projects, tasks and users from backup xml file. " +
            "Runs automatically on application start. Can be run manually.";

    {
        setNeedAuthorization(false);
    }

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return Role.values();
    }

    @Override
    @SneakyThrows
    public void execute() {
        if (!new File(FILE_SAVED_DATA).isFile()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_SAVED_DATA)));
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final Domain domain = xmlMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
