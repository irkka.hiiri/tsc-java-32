package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readLine;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "show project by name";

    @NotNull
    public final static String DESCRIPTION = "show project by name";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        @NotNull final String name = readLine(NAME_INPUT);
        @Nullable final Project project = getProjectService().findByName(name);
        showProject(project);
    }

}
