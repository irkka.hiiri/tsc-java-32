package ru.tsc.ichaplygina.taskmanager.command.domain;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.dto.Domain;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;

import java.io.FileOutputStream;

public class DomainSaveBackupCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "save backup";

    @NotNull
    public final static String DESCRIPTION = "save projects, tasks and users to backup xml file " +
            "which is loaded on application start. Runs automatically and can be run manually.";

    {
        setNeedAuthorization(false);
    }

    @NotNull
    @Override
    public String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final Role[] getRoles() {
        return Role.values();
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final XmlMapper xmlMapper = new XmlMapper();
        @NotNull final String xml = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_SAVED_DATA);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
