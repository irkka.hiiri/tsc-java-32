package ru.tsc.ichaplygina.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.model.Project;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.readNumber;

public final class ProjectShowByIndexCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "show project by index";

    @NotNull
    public final static String DESCRIPTION = "show project by index";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        final int index = readNumber(INDEX_INPUT);
        @Nullable final Project project = getProjectService().findByIndex(index - 1);
        showProject(project);
    }

}
