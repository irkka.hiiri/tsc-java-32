package ru.tsc.ichaplygina.taskmanager.command.user;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.exception.security.AccessDeniedNotAuthorizedException;

import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printLinesWithEmptyLine;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class WhoAmICommand extends AbstractUserCommand {

    @NotNull
    public static final String NAME = "whoami";

    @NotNull
    public static final String DESCRIPTION = "print current user login";

    @NotNull
    @Override
    public final String getCommand() {
        return NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public final void execute() {
        if (isEmptyString(getAuthService().getCurrentUserId())) throw new AccessDeniedNotAuthorizedException();
        printLinesWithEmptyLine(getUserService().findById(getAuthService().getCurrentUserId()));
    }

}
