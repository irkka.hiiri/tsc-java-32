package ru.tsc.ichaplygina.taskmanager.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.command.AbstractCommand;

import static ru.tsc.ichaplygina.taskmanager.constant.StringConst.APP_HELP_HINT_TEXT;
import static ru.tsc.ichaplygina.taskmanager.util.TerminalUtil.printList;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    public final static String CMD_NAME = "help";

    @NotNull
    public final static String ARG_NAME = "-h";

    @NotNull
    public final static String DESCRIPTION = "show this message";

    @NotNull
    @Override
    public final String getCommand() {
        return CMD_NAME;
    }

    @NotNull
    @Override
    public final String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public final String getArgument() {
        return ARG_NAME;
    }

    @Override
    public final void execute() {
        System.out.println(APP_HELP_HINT_TEXT);
        printList(getCommandService().getCommandList());
    }

}
