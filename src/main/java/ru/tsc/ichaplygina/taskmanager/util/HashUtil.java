package ru.tsc.ichaplygina.taskmanager.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.property.IPasswordProperty;

import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

@UtilityClass
public final class HashUtil {

    private static final int ITERATION = 66666;
    @NotNull
    private static final String SECRET = "2u34hr3wueu3wfd";

    @Nullable
    public static String md5(@NotNull final String value) {
        if (isEmptyString(value)) return null;
        try {
            @NotNull final java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            @NotNull byte[] array = md.digest(value.getBytes());
            @NotNull final StringBuilder sb = new StringBuilder();
            for (final byte b : array) {
                sb.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (@NotNull java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    public static String salt(@Nullable final String value, @NotNull final IPasswordProperty passwordProperty) {
        final int iteration = passwordProperty.getPasswordIteration();
        @NotNull final String secret = passwordProperty.getPasswordSecret();
        if (isEmptyString(value)) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) result = md5(secret + result + secret);
        return result;
    }

}
